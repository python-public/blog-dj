# Приложение для блога
## Django Projects

<ul><b>Блог</b>
<li>Создание моделей и представлений URL-адресов</li>
<li>Добавление и настройка свойств для администратора</li>
<li>Использование canonical URL-адресов для SEO-оптимизации</li>
<li>Добавление пагинации</li>
<li>Работа с Django-forms</li>
<li>Добавление тегов к постам с помощью django-taggit</li>
<li>Рекомендация постов на основе общих тегов</li>
<li>Пользовательские фильтры шаблонов с помощью Markdown</li>
<li>Создание карты сайта и RSS-ленты</li>
<li>Подключение к PostgreSQL</li>
<li>Реализована поисковая строка с использованием PostgreSQL</li>
</ul>

## Запуск приложения
<ol>
<li>pip install -r requirements.txt</li>
<li>Подключение PostgreSQL</li>
<li>python manage.py makemigrations</li>
<li>python manage.py migrate</li>
<li>python manage.py runserver_plus --cert-file cert.crt</li>
</ol>